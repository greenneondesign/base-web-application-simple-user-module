<?php
	namespace Users;

	class User implements \JsonSerializable {
		private static $authenticated_user;	//storing the user in the Session leads to a cache invalidation attack
		
		public $id;
		public $given_name;
		public $family_name;
		public $email;
		public $is_admin;
		public $is_active;
		
		/**
		 *	Build an invalid User. PHP does not allow constructor overloading so
		 *	we will default to something that is obviously broken and provide a
		 *	factory method for constructing instances from data.
		 */
		public function __construct() {
			$this->id = -1;
			$this->given_name = '';
			$this->family_name = '';
			$this->email = '';
			$this->is_admin = false;
			$this->is_active = false;
		}
		
		/**
		 *	Creates a User from data
		 */
		private static function build($id, $given_name, $family_name, $email, $is_admin, $is_active) {
			$user = new User();
			$user->id = $id;
			$user->given_name = $given_name;
			$user->family_name = $family_name;
			$user->email = $email;
			$user->is_admin = $is_admin;
			$user->is_active = $is_active;
			
			
			return $user;
		}
		
		/**
		 *	is_user_authenticated - get the currently authenticated user
		 *
		 *	@return true if and only if the current user has authenticated, otherwise false
		 */
		public static function is_user_authenticated() {
			return empty($_SESSION['user']) ? false : true;
		}
		
		/**
		 *	get_authenticated_user - get the currently authenticated user
		 *
		 *	@return the currently authenticated user or NULL is the user is
		 *		not currently authenticated
		 */
		public static function get_authenticated_user() {
			if(NULL === $_SESSION['user']) {
				return NULL;
			} else {
				if(self::$authenticated_user) {
					return self::$authenticated_user;
				} else {
					//pull user from database much like load except no is_authenticated check
					$connection = \DB::getConnection();
					$sql = 'SELECT id, given_name, family_name, email, is_admin, is_active FROM users WHERE id = :id';
					$query = $connection->prepare($sql);
					if($query->execute(array('id' => $_SESSION['user']))) {
						// got a result... check that user is still active
						$row = $query->fetch(\PDO::FETCH_ASSOC);
						if($row['is_active']) {
							self::$authenticated_user = User::build($row['id'], $row['given_name'], $row['family_name'], $row['email'], $row['is_admin'], $row['is_active']);
							return self::$authenticated_user;
						} else {	// user has been deactivated
							// Notify??
							session_regenerate_id();
							return NULL;
						}
					} else {
						\Notification::error($query->errorInfo()[2]);
						return NULL;
					}
				}
			}
		}
		
		/**
		 *	get_display_name - get the human readable name for the user as a string
		 *
		 *	@return a string representing the human readable name of the user
		 */
		public function get_display_name() {
			return $this->given_name . ' ' . $this->family_name;
		}
		
		/**
		 *	Pull a list of all users subject to sorting and filtering from the db 
		 *
		 *	@param filters - an array of key value pairs to filter the search
		 *	@param sort - a string representing the name of a column to sort upon
		 *	@param order - a string representing the sort order
		 *	@param page_size - an integer representing the pagination page_size
		 *	@param page - an integer representing the pagination page (offset)
		 *
		 *	@return an array of two values, the 0th being the total number of users
		 *		passing the filters and the second being the array of Users that passed
		 *		filtering and pagination
		 */
		public static function all($filters = NULL, $sort = NULL, $order = NULL, $page_size = NULL, $page = NULL) {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}

			$connection = \DB::getConnection();
			
			// Setup Filtering... we don't do any currently
			$where_clause = '';
			
			// Setup Pagination
			$limit_clause = '';
			$offset = null;
			$total_search_results = null;
			if($page_size) {
				$sql = 'SELECT count(*) FROM users';
				$query = $connection->prepare($sql . $where_clause);
//				if(is_array($filters)) {	// apply filters
//					
//				}
				if($query->execute()) {
					$total_search_results = $query->fetchColumn();
				} else {
					\Notification::error($query->errorInfo()[2]);
				}
				if($total_search_results > 0 && NULL != $page_size && is_numeric($page_size) && $page_size > 0) {
					//we need to return a slice of the search results with page_size entries
					if(NULL != $page && is_numeric($page) && $page > 0) {
						$offset = $page * $page_size;
						if($page * $page_size < $total_search_results) {
							$limit_clause = ' LIMIT :page_size OFFSET :offset';
						} else {	// someone being cute and trying to read past the array bounds?
							throw new \BadRequestException();
						}
					} else {	// page 0 or no offset; show first page
						$limit_clause = ' LIMIT :page_size';
					}
				}
			}
			
			$sql = 'SELECT id, given_name, family_name, email, is_admin, is_active FROM users';
			
			// Sorting
			$order_clause = ' ORDER BY id';
			if(NULL != $sort) {
				switch($sort) {
					case 'active':
						$order_clause = ' ORDER BY is_active';
						break;
					case 'given_name':
						$order_clause = ' ORDER BY given_name';
						break;
					case 'family_name':
						$order_clause = ' ORDER BY family_name';
						break;
				}
				
				if(NULL != $order) {
					switch($order) {
						case 'asc':
							$order_clause .= ' ASC';
							break;
						case 'desc':
							$order_clause .= ' DESC';
							break;
					}
				}
			}
			
			$query = $connection->prepare($sql . $where_clause . $order_clause . $limit_clause);
			
			// Apply Pagination
			if($total_search_results > 0 && $page_size) {
				$query->bindValue(':page_size', $page_size, \PDO::PARAM_INT);
			}
			
			if($offset) {
				$query->bindValue(':offset', $offset, \PDO::PARAM_INT);
			}
			
			$users = array();
			if($query->execute()) {
				while($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					$user = User::build($row['id'], $row['given_name'], $row['family_name'], $row['email'], $row['is_admin'], $row['is_active']);
					$users[] = $user;
				}
			} else {
				\Notification::error($query->errorInfo()[2]);
			}

			return array($total_search_results, $users);
		}
		
		/**
		 *	Loads a specific user from the DB
		 */
		public static function load($id) {
			if(!User::is_user_authenticated()) {	// if not logged in only, users can see who owns other objects
				throw new \NotAuthorizedException();
			}
			
			if(NULL === $id) {
				// we may be doing a lookup on a foreign key that is not yet set... this is not an error
				return NULL;
			}
			
			$connection = \DB::getConnection();
			$sql = 'SELECT id, given_name, family_name, email, is_admin, is_active FROM users WHERE id = :id';
			$query = $connection->prepare($sql);
			if($query->execute(array('id' => $id))) {
				if($row = $query->fetch(\PDO::FETCH_ASSOC)) {
					return User::build($row['id'], $row['given_name'], $row['family_name'], $row['email'], $row['is_admin'], $row['is_active']);
				} else {
					\Notification::log('User:' . $id . ' could not be found');
					return NULL;
				}
			} else {
				\Notification::error($query->errorInfo()[2]);
				return NULL;
			}
		}
		
		private function validateUserFromFormFields() {
			$retval = true;
			
			if(class_exists('\CSRF')) {
				if(!\CSRF::validate()) {
					\Notification::log("The request was rejected because it appears to originate from a Cross Site Request Forgery. Please try again.");
					$retval = false;
				}
			}
			
			if(!isset($_POST['given_name']) || empty($_POST['given_name'])) {
				\Notification::log("The user's given name is required");
				$retval = false;
			} else {
				$this->given_name = strip_tags($_POST['given_name']);
			}
			
			if(!isset($_POST['family_name']) || empty($_POST['family_name'])) {
				\Notification::log("The user's family name is required");
				$retval = false;
			} else {
				$this->family_name = strip_tags($_POST['family_name']);
			}
			
			if(!isset($_POST['email']) || empty($_POST['email'])) {
				\Notification::log('The user must have an email address so that they may log in');
				$retval = false;
			} else {
				// need to check that user name is not already in db when creating or changing to a value in another record upon updating; we'll do so by making the field Unique in the DB so we get an error there
				$this->email = strip_tags($_POST['email']);
			}
			
			if(isset($_POST['admin']) && !empty($_POST['admin'])) {
				$this->is_admin = true;
			} else {
				$this->is_admin = false;
			}
			
			if(isset($_POST['active']) && !empty($_POST['active'])) {
				$this->is_active = true;
			} else {
				$this->is_active = false;
			}

			return $retval;
		}
		
		/**
		 *	Create a new random password for our user, hash it for storage and push
		 * 	the plaintext onto the Notification stack
		 */
		private function generatePassword() {
			// DANGER HARDCODED VALUES!!!
			$chars = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789!@#$%^&*-+';
			$length = 12;
			
			$len = strlen($chars)-1;
			$pw = '';
 
			for($i=0;$i<$length;$i++) {
				$pw .= $chars[rand(0, $len)];
			}
 
			$pw = str_shuffle($pw);
			
			\Notification::log('The password: <strong>' . $pw . '</strong> was generated for the user');
			return password_hash($pw, PASSWORD_DEFAULT);
		}
		
		/**
		 *	Administrative method to reset a user's password to a random password or
		 *		something chosen.
		 *
		 *	@env POST('chooseRandom') - whether to choose a random password will be
		 *						set if choosing and unset if using a chosen password
		 *	@env @optional POST('password') - the chosen password to set must be a
		 *						character string if POST('chooseRandom') is not set
		 */
		public function resetPassword() {
			// if not logged in or (not an admin or the user loading their own record)
			if(!User::is_user_authenticated() || !($this->id === User::get_authenticated_user()->id || User::get_authenticated_user()->has_permission('admin'))) {
				throw new \NotAuthorizedException();
			}
			
			$password_hash = '';
			
			if(class_exists('\CSRF')) {
				if(!\CSRF::validate()) {
					\Notification::log("The request was rejected because it appears to originate from a Cross Site Request Forgery. Please try again.");
					return;
				}
			}
			
			// if POST('chooseRandom') then call generate password
			if(isset($_POST['chooseRandom']) && !empty($_POST['chooseRandom'])) {
				$password_hash = $this->generatePassword();
			} else {
				// DANGER HARDCODED VALUES!!! should be a policy setting for the password strength
				if(isset($_POST['password']) && !empty($_POST['password']) && strlen($_POST['password']) >= 8) {
					// hash it
					$password_hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
				} else {
					// report an error
					\Notification::error('The password chosen did not meet the strictures of the security policy (Length of eight or more characters). Password is unchanged.');
					return;
				}
			}
			
			//now save the password hash
			$connection = \DB::getConnection();
			$sql = 'UPDATE users SET password_hash = :password_hash where id = :id';
			$query = $connection->prepare($sql);
			if($query->execute(array(	'id' => $this->id,
										'password_hash' => $password_hash))) {
				\Notification::success('The password has been changed for ' . $this->given_name . ' ' . $this->family_name);
			} else {
				\Notification::error($query->errorInfo()[2]);
			}
		}
		
		/**
		 *	saves a newly created user to the database
		 */
		public function save() {
			// if not logged in or (not an admin or the user saving their own record)
			if(!User::is_user_authenticated() || !($this->id === User::get_authenticated_user()->id || User::get_authenticated_user()->has_permission('admin'))) {
				throw new \NotAuthorizedException();
			}
			
			$connection = \DB::getConnection();
			if($this->id < 0) { // a new user SQL::insert
				$sql = 'INSERT INTO users(given_name, family_name, email, is_admin, is_active, created_by, last_updated_by) VALUES(:given_name, :family_name, :email, :is_admin, :is_active, :creator, :creator)';
				$query = $connection->prepare($sql);
				$query->bindValue(':creator', User::get_authenticated_user()->id, \PDO::PARAM_INT);
			} else { // an existing user, SQL::update
				$sql = 'UPDATE users SET given_name = :given_name, family_name = :family_name, email = :email, is_admin = :is_admin, is_active = :is_active, last_updated_by = :last_updated_by, last_updated_date = CURRENT_TIMESTAMP where id = :id';
				$query = $connection->prepare($sql);
				$query->bindValue(':last_updated_by', User::get_authenticated_user()->id, \PDO::PARAM_INT);
				$query->bindValue(':id', $this->id, \PDO::PARAM_INT);
			}

			$query->bindValue(':given_name', $this->given_name);
			$query->bindValue(':family_name', $this->family_name);
			$query->bindValue(':email', $this->email);
			$query->bindValue(':is_admin', $this->is_admin, \PDO::PARAM_BOOL);
			$query->bindValue(':is_active', $this->is_active, \PDO::PARAM_BOOL);
			
			if($query->execute()) {
				if($this->id < 0) {
					$this->id = $connection->lastInsertId('users_id_seq');
					\Notification::success('User Created');
				} else {
					\Notification::success('User Saved');
				}
			} else {
				\Notification::error($query->errorInfo()[2]);
			}
		}

		public function create() {		
			return User::validateUserFromFormFields();
		}
		
		public function update() {
			return User::validateUserFromFormFields();
		}
		
		/**
		 *	Looks at POST input to determine if it represents a valid and active user
		 *	credential
		 *
		 *	@sideeffect regenerates the SESSION cookie
		 *	@sideeffect places the User object representing the authenticated user in
		 *		$SESSION 
		 *	@return	true if user is logged in, false otherwise
		 */
		public static function validateLogin() {			
			// check for input
			if(!isset($_POST['username']) || empty($_POST['username']) || !isset($_POST['password']) || empty($_POST['password'])) {
				return false;
			}
			
			//pull user from database
			$connection = \DB::getConnection();
			$sql = 'SELECT id, given_name, family_name, email, password_hash, is_admin, is_active FROM users WHERE email = :username';
			$query = $connection->prepare($sql);
			if($query->execute(array('username' => $_POST['username']))) {
				// got a result... compare password hashes
				$result = $query->fetch(\PDO::FETCH_ASSOC);
				if(password_verify($_POST['password'], $result['password_hash']) && $result['is_active']) {
					session_regenerate_id();
					$user = User::build($result['id'], $result['given_name'], $result['family_name'], $result['email'], $result['is_admin'], $result['is_active']);
					$_SESSION['user'] = $user->id;
					return true;
				} else {
					\Notification::log('Could not find user or password was incorrect');
					return false;
				}
			} else {
				\Notification::error($query->errorInfo()[2]);
				return false;
			}
		}
		
		public function has_permission($permission) {
			switch($permission) {
				case 'admin':
					return $this->is_admin;
				default:
					return false;
			}
		}
		
		/**
		 *	Restrict fields reported in json output based on user privileges
		 */
		public function jsonSerialize() {
			//Fields for all users
			$serialized = array('id' => $this->id, 'given_name' => $this->given_name, 'family_name' => $this->family_name);

			// Fields for authenticated users
			if(User::is_user_authenticated()) {
				$serialized['email'] = $this->email;
			}
			
			// Fields for Administrators
			if(User::is_user_authenticated() && User::get_authenticated_user()->has_permission('admin')) {
				$serialized['is_admin'] = $this->is_admin;
				$serialized['is_active'] = $this->is_active;
			}
			return $serialized;
		}
	}
?>