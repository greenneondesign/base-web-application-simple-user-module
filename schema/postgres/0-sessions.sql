CREATE TABLE IF NOT EXISTS sessions (
	id varchar(40) PRIMARY KEY,
	last_access timestamp DEFAULT now() NOT NULL,
	data text
);