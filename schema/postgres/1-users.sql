CREATE TABLE IF NOT EXISTS users (
	id serial PRIMARY KEY,
	given_name text,
	family_name text,
	email text UNIQUE,
	password_hash text,
	is_admin boolean,
	is_active boolean,
	created_by integer NOT NULL,
	created_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
	last_updated_by integer NOT NULL,
	last_updated_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
);

ALTER TABLE users
	ADD CONSTRAINT fk_users_created_by FOREIGN KEY (created_by) REFERENCES users(id);

ALTER TABLE users
	ADD CONSTRAINT fk_users_last_updated_by FOREIGN KEY (last_updated_by) REFERENCES users(id);