				<ul class="breadcrumbs">
					<li><a href="/users/">Users</a></li>
					<li><span>New User</span></li>
				</ul>
				<article>
					<h1>New User</h1>
					<form method="post">
<?php if(class_exists('\CSRF')): ?>
						<?php \CSRF::emit(); ?>
<?php endif; ?>
						<div class="form-field text"><label for="given_name">Given Name</label><input type="text" id="given_name" name="given_name" value="<?=isset($user) ? $user->given_name : '' ?>"></div>
						<div class="form-field text"><label for="family_name">Family Name</label><input type="text" name="family_name" id="family_name" value="<?=isset($user) ? $user->family_name : '' ?>"></div>
						<div class="form-field text"><label for="email">Email Address</label><input type="email" name="email" id="email" value="<?=isset($user) ? $user->email : '' ?>"></div>
						<div class="form-field check-box"><label for="admin">Administrator</label><input type="checkbox" name="admin" id="admin"></div>
						<div class="form-field check-box"><label for="active">Active</label><input type="checkbox" name="active" id="active" checked></div>
						<div class="button-group">
							<a href="/users/">Cancel</a><button type="submit">Create</button>
						</div>
					</form>
				</article>
