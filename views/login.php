				<div class="dialog">
					<form method="post">
						<div class="form-field"><label for="username">Email</label><input type="email" name="username" id="username" required autofocus></div>
						<div class="form-field"><label for="password">Password</label><input type="password" name="password" id="password" required></div>
						<div class="button-group">
							<button type="submit">Login</button>
						</div>
					</form>
				</div>
