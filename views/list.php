				<article>
					<header>
						<h1>Users</h1>
						<div class="button-group">
							<a href="/users/create/">New User</a>
						</div>
					</header>
					<table>
						<thead>
							<tr><th>&nbsp;</th><th><a href="/users/?sort=given_name">Given Name</a></th><th><a href="/users/?sort=last_name">Last Name</a></th><th><a href="/users/?sort=is_active">Active</a></th></tr>
						</thead>
<?php	if(count($users) > 0): ?>
						<tbody>
<?php		foreach($users as $user): ?>
							<tr>
								<td><a href="/users/read/<?=$user->id?>" title="User details" aria-label="User details"><i class="fa fa-user" aria-hidden="true"></i></a></td>
								<td><a href="/users/read/<?=$user->id?>" title="User details" aria-label="User details"><?=$user->given_name?></a></td>
								<td><a href="/users/read/<?=$user->id?>" title="User details" aria-label="User details"><?=$user->family_name?></a></td>
								<td style="text-align:center"><a href="/users/read/<?=$user->id?>" title="User details" aria-label="User details"><?=$user->is_active ? '<i class="fa fa-check-square-o" aria-hidden="true"></i>' : '<i class="fa fa-square-o" aria-hidden="true"></i>' ?></a></td>
							</tr>
<?php		endforeach; ?>
						</tbody>
<?php		if($total_search_results > $page_size): ?>
						<tfoot>
							<td colspan="4">
<?=\Theme::theme()->pagination_links('/users/', $base_query, $total_search_results, $page_size, $page) ?>
							</td>
						</tfoot>
<?php		endif; ?>
<?php	else: ?>
						<tbody>
							<tr><td colspan="4">No users matching the search criteria were found.</td></tr>
						</tbody>
<?php	endif; ?>
					</table>
				</article>
