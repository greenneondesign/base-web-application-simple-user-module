				<ul class="breadcrumbs">
					<li><a href="/users/">Users</a></li>
					<li><span><?=$user->given_name ?> <?=$user->family_name ?></span></li>
				</ul>
				<article>
					<script>
						function toggleChoosePassword() {
							var passwordField = document.getElementById('password');
							passwordField.disabled = !passwordField.disabled;
						}
					</script>
					<h1><?=$user->given_name ?> <?=$user->family_name ?></h1>
					<dl>
						<dt>Name</dt><dd><?=$user->given_name ?> <?=$user->family_name ?></dd>
						<dt>Email</dt><dd><?=$user->email ?></dd>
						<dt>Administrator</dt><dd><?=$user->is_admin ? 'Yes' : 'No' ?></dd>
						<dt>Active</dt><dd><?=$user->is_active ? 'Yes' : 'No' ?></dd>
					</dl>
					<div class="button-group">
						<a href="/users/update/<?=$user->id ?>">Edit</a><label for="password-dialog-toggle" />Change Password</label>
					</div>
					<input type="checkbox" class="checkbox-hack" id="password-dialog-toggle">
					<div class="in-page-popover" id="PasswordDialog">
						<label for="password-dialog-toggle" class="scrim"></label>
						<div class="dialog">
							<form method="post" action="/users/reset_password/<?=$user->id ?>">
<?php if(class_exists('\CSRF')): ?>
								<?php \CSRF::emit(); ?>
<?php endif; ?>
								<h1>Change Password</h1>
								<div class="form-field check-box"><label for="chooseRandom">Choose a password for me</label><input type="checkbox" id="chooseRandom" name="chooseRandom" checked="checked" onclick="toggleChoosePassword()" /></div>
								<p style="text-align:center">&mdash; or &mdash;</p>
								<div class="form-field text"><label for="password">Password</label><input type="password" id="password" name="password" disabled="disabled"></div>
								<div class="button-group">
									<label for="password-dialog-toggle">Cancel</label><button type="submit">Change Password</button>
								</div>
							</form>
						</div>
					</div>
				</article>
