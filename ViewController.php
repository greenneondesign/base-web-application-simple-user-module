<?php
	namespace Users;
	
	include_once('Model.php');

	class ViewController implements \ViewController {

		/**
		 *	_navigation_slug returns the text representation for a link to the default
		 *		(automatic) action of this module. Useful when dynamically creating
		 *		navigation elements
		 *
		 *	@return - text for a link to this module's default action
		 */
		public static function _navigation_slug() {
			return '<i class="fa fw fa-users" aria-hidden="true"></i> Manage Users';
		}
		
		/**
		 *	_visible indicates whether it is reasonable for this module to appear in
		 *		navigation elements
		 *
		 *	@return - boolean true if this module's default action should be present
		 *		in navigation elements and false otherwise.
		 */
		public static function _visible() {
			if(User::is_user_authenticated()) {
				if(User::get_authenticated_user()->has_permission('admin')) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		
		/**
		 *	automatic - If the user is an administrator they will be presented with
		 *		the user list, if they are not an administrator they will be presented
		 *		with their profile.
		 *
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function automatic() {
			if(User::is_user_authenticated()) {
				if(User::get_authenticated_user()->has_permission('admin')) {
					ViewController::index();
				} else {
					ViewController::profile();
				}
			} else {
				throw new \NotAuthorizedException();
			}
		}
		
		/**
		 *	index presents a listing of Users while "list" would be a more intuitive
		 *		name for this method, it happens to be a reserved word in PHP
		 *
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function index() {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			//$filters = array();	// we don't yet support filtering here
			$sort = NULL;
			if(isset($_GET['sort']) && !empty($_GET['sort'])) {
				$sort = strip_tags($_GET['sort']);
			}
			
			$order = NULL;
			if(isset($_GET['order']) && !empty($_GET['order'])) {
				$order = strip_tags($_GET['order']);
			}
			
			$page_size = 10;	// set a default pagination page size
			if(isset($_GET['results_per_page']) && !empty($_GET['results_per_page'])) {
				$page_size = strip_tags($_GET['results_per_page']);
			}
			
			$page = 0;
			if(isset($_GET['page']) && !empty($_GET['page'])) {
				$page = strip_tags($_GET['page']);
			}

			//$base_query = http_build_query($filters);
			$base_query = NULL;
			list($total_search_results, $users) = User::all(NULL, $sort, $order, $page_size, $page);	
			\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'list.php', array('users' => $users, 'total_search_results' => $total_search_results, 'page_size' => $page_size, 'page' => $page, 'base_query' => $base_query));
		}
		
		/**
		 *	create - presents a form for collecting the information to create a user
		 *		and if acceptable information is presented creates the user and redirects
		 *		to the User list action
		 *
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function create() {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			if(!empty($_POST)) { // do we have input?
				$user = new User();
				if($user->create()) {
					$user->save();
					\Application::application()->redirect('/users/');
				} else {
					\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'create.php', array('user' => $user));
				}
			} else {
				\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'create.php');
			}
		}
		
		/**
		 *	update - presents a form for collecting the information to modify an
		 *		existing user and if acceptable information is presented modifies the
		 *		user and redirects to the User view action
		 *
		 *	@param id - the id of the user to modify
		 *
		 *	@throws BadRequestException if the id does not correspond to an existing
		 *		user
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function update($id = NULL) {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			if(NULL === $id) {
				throw new \BadRequestException();
			}
			
			if(!empty($_POST)) { // do we have input?
				$user = User::load($id);
				if($user->update()) {
					$user->save();
					\Application::application()->redirect('/users/read/' . $id);
				} else {
					// Notify of error
					\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'update.php', array('user' => $user));
				}
			} else {
				$user = User::load($id);
				if($user) {
					\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'update.php', array('user' => $user));
				} else {
					throw new \BadRequestException();
				}
			}
		}
		
		/**
		 *	read - presents information on a User to the requestor
		 *
		 *	@param id - the id of the user to view
		 *
		 *	@throws BadRequestException if the id does not correspond to an existing
		 *		user
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function read($id = NULL) {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}
			
			if(NULL === $id) {
				throw new \BadRequestException();
			}
			
			$user = User::load($id);
			if($user) {
				\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'view.php', array('user' => $user));
			} else {
				throw new \BadRequestException();
			}
		}
		
		/**
		 *	reset_password - resets the specified User's password either using a
		 *		supplied password or by choosing a random password that will be reported
		 *		back in a notification
		 *
		 *	@param id - the id of the user to reset the password for
		 *
		 *	@throws BadRequestException if the id does not correspond to an existing
		 *		user
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function reset_password($id = NULL) {
			if(!User::is_user_authenticated() || !User::get_authenticated_user()->has_permission('admin')) {
				throw new \NotAuthorizedException();
			}

			if(NULL === $id) {
				throw new \BadRequestException();
			}
			
			$user = User::load($id);
			if($user) {
				$user->resetPassword();
				\Application::application()->redirect('/users/read/' . $id);
			} else {
				ViewController::index();
			}
		}
		
		/**
		 *	profile - a special case of read, the authenticated user can read their
		 *		representation as a User
		 *
		 *	@throws NotAuthorizedException if the current user is unauthenticated or
		 *		does not have the desired permission level to see the user list
		 */
		public static function profile() {
			if(!User::is_user_authenticated()) {
				throw new \NotAuthorizedException();
			}
			
			\Theme::theme()->render('views' . DIRECTORY_SEPARATOR . 'profile.php', array('user' => User::get_authenticated_user()));
		}
		
		/**
		 *	change_password - a special case of reset_password, the authenticated user
		 *		can change their password
		 *
		 *	@throws NotAuthorizedException if the current user is unauthenticated
		 */
		public static function change_password() {
			if(!User::is_user_authenticated()) {
				throw new \NotAuthorizedException();
			}
			
			User::get_authenticated_user()->resetPassword();
			\Application::application()->redirect('/users/profile/');
		}
		
		/**
		 *	login - displays the login form or if the user presents valid credentials
		 *		adds the User representation to $_SESSION in effect marking the bear of
		 *		the session token as an authorized user
		 */
		public static function login() {
			if(User::validateLogin()) {
				\Application::application()->redirect();
			} else {
				\Theme::theme()->render_static('login.php', __DIR__ . DIRECTORY_SEPARATOR . 'views');
			}
		}
		
		/**
		 *	logout - pulls the user's representation out of $_SESSION marking the
		 *		session token bearer as unauthorized and redirects to the default route.
		 *		Also revokes any csrf secret.
		 */
		public static function logout() {
			/*
			 *	It is conceivable that a module might store something in the $_SESSION
			 *	It would be better if not (PHP < 7, they will go away on login anyways)
			 *	but just in case we have to selectively unset values rather than looping
			 *	through and unsetting all. Fortunately unset does not throw errors if
			 *	something is not set so this is nice and safe
			 */
			unset($_SESSION['csrf_secret']);
			unset($_SESSION['user']);
			\Application::application()->redirect();
		}
	}
?>